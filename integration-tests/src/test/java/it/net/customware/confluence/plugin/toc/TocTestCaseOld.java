package it.net.customware.confluence.plugin.toc;

import java.util.List;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import org.xml.sax.SAXException;

import static java.util.Arrays.asList;

/**
 * Old test case for the pure server-side implementation of the TOC macro. Preserved here for posterity and amusement.
 */
public abstract class TocTestCaseOld extends AbstractConfluencePluginWebTestCase
{
    protected TocTester tocTester;
    protected String testSpaceKey;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        tocTester = new TocTester(tester);
        testSpaceKey = tocTester.createTestSpace();
    }

    public void testCreateTocWithNonSequentialHeadersStartingFromH2()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with non-sequential headings starting from h2");
        pageHelper.setContent(
                "{toc}\n" +
                        "h2. Heading 1\n" +
                        "h4. Heading 1.1\n" +
                        "h4. Heading 1.2\n" +
                        "h6. Heading 1.2.1\n" +
                        "h2. Heading 2\n" +
                        "h4. Heading 2.1\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+non-sequential+headings+starting+from+h2");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading(
                                "Heading 1",
                                "#TOCwithnon-sequentialheadingsstartingfromh2-Heading1",
                                asList(
                                        new Heading(
                                                "Heading 1.1",
                                                "#TOCwithnon-sequentialheadingsstartingfromh2-Heading1.1"
                                        ),
                                        new Heading(
                                                "Heading 1.2",
                                                "#TOCwithnon-sequentialheadingsstartingfromh2-Heading1.2",
                                                asList(
                                                        new Heading(
                                                                "Heading 1.2.1",
                                                                "#TOCwithnon-sequentialheadingsstartingfromh2-Heading1.2.1"
                                                        )
                                                )
                                        )
                                )
                        ),
                        new Heading(
                                "Heading 2",
                                "#TOCwithnon-sequentialheadingsstartingfromh2-Heading2",
                                asList(
                                        new Heading(
                                                "Heading 2.1",
                                                "#TOCwithnon-sequentialheadingsstartingfromh2-Heading2.1"
                                        )
                                )
                        )
                )
        );
    }

    public void testCreateOutlinedTocWithNonSequentialHeadersStartingFromH2()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Outlined TOC with non-sequential headings starting from h2");
        pageHelper.setContent(
                "{toc:outline=true}\n" +
                        "h2. Heading 1\n" +
                        "h4. Heading 1.1\n" +
                        "h4. Heading 1.2\n" +
                        "h6. Heading 1.1.1\n" +
                        "h2. Heading 2\n" +
                        "h4. Heading 2.1\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Outlined+TOC+with+non-sequential+headings+starting+from+h2");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading(
                                "1",
                                "Heading 1",
                                "#OutlinedTOCwithnon-sequentialheadingsstartingfromh2-Heading1",
                                asList(
                                        new Heading(
                                                "1.1",
                                                "Heading 1.1",
                                                "#OutlinedTOCwithnon-sequentialheadingsstartingfromh2-Heading1.1"
                                        ),
                                        new Heading(
                                                "1.2",
                                                "Heading 1.2",
                                                "#OutlinedTOCwithnon-sequentialheadingsstartingfromh2-Heading1.2",
                                                asList(
                                                        new Heading(
                                                                "1.2.1",
                                                                "Heading 1.1.1",
                                                                "#OutlinedTOCwithnon-sequentialheadingsstartingfromh2-Heading1.1.1"
                                                        )
                                                )
                                        )
                                )
                        ),
                        new Heading(
                                "2",
                                "Heading 2",
                                "#OutlinedTOCwithnon-sequentialheadingsstartingfromh2-Heading2",
                                asList(
                                        new Heading(
                                                "2.1",
                                                "Heading 2.1",
                                                "#OutlinedTOCwithnon-sequentialheadingsstartingfromh2-Heading2.1"
                                        )
                                )
                        )
                )
        );
    }

    public void testCreateTocWithHeadingsContainingWikiMarkup()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with wiki markup in headings");
        pageHelper.setContent(
                "{toc}\n" +
                        "h2. [Confluence Overview|ds:Confluence Overview]\n" +
                        "h4. {color:red}Red heading{color}\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+wiki+markup+in+headings");


        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading(
                                "Confluence Overview",
                                "#TOCwithwikimarkupinheadings-ConfluenceOverview",
                                asList(
                                        new Heading(
                                                "Red heading",
                                                "#TOCwithwikimarkupinheadings-Redheading"
                                        )
                                )
                        )
                )
        );
    }

    public void testCreateTocWithHeadingsExcluded()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with excluded heading");
        pageHelper.setContent(
                "{toc:exclude=Heading two}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+excluded+heading");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading(
                                "Heading one",
                                "#TOCwithexcludedheading-Headingone"
                        )
                )
        );
    }

    public void testCreateTocWithHeadingsIncluded()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with included heading");
        pageHelper.setContent(
                "{toc:include=Heading one}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+included+heading");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading(
                                "Heading one",
                                "#TOCwithincludedheading-Headingone"
                        )
                )
        );
    }

    public void testCreateTocWithCssClassSpecified()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with CSS class specified");
        pageHelper.setContent(
                "{toc:class=foobar}\n" +
                        "h2. Heading 1\n" +
                        "h4. Heading 1.1\n" +
                        "h4. Heading 1.2\n" +
                        "h6. Heading 1.2.1\n" +
                        "h2. Heading 2\n" +
                        "h4. Heading 2.1\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+CSS+class+specified");



        verifyHeadings(
                "//div[@class='wiki-content']/div[@class='toc-macro foobar']/ul",
                asList(
                        new Heading(
                                "Heading 1",
                                "#TOCwithCSSclassspecified-Heading1",
                                asList(
                                        new Heading(
                                                "Heading 1.1",
                                                "#TOCwithCSSclassspecified-Heading1.1"
                                        ),
                                        new Heading(
                                                "Heading 1.2",
                                                "#TOCwithCSSclassspecified-Heading1.2",
                                                asList(
                                                        new Heading(
                                                                "Heading 1.2.1",
                                                                "#TOCwithCSSclassspecified-Heading1.2.1"
                                                        )
                                                )
                                        )
                                )
                        ),
                        new Heading(
                                "Heading 2",
                                "#TOCwithCSSclassspecified-Heading2",
                                asList(
                                        new Heading(
                                                "Heading 2.1",
                                                "#TOCwithCSSclassspecified-Heading2.1"
                                        )
                                )
                        )
                )
        );
    }

    public void testCreateFlatTocWithBracketSeparator()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Flat TOC");
        pageHelper.setContent(
                "{toc:type=flat|separator=bracket}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Flat+TOC");

        String tocDivXPath = "//div[@class='wiki-content']/div";

        verifyFlatHeadings(
                tocDivXPath,
                asList(
                        new Heading("Heading one", "#FlatTOC-Headingone"),
                        new Heading("Heading two", "#FlatTOC-Headingtwo")
                )
        );
        assertEquals("[ Heading one ] [ Heading two ]", getElementTextByXPath(tocDivXPath));
    }

    public void testCreateFlatTocWithBraceSeparator()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Flat TOC");
        pageHelper.setContent(
                "{toc:type=flat|separator=brace}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Flat+TOC");

        String tocDivXPath = "//div[@class='wiki-content']/div";

        verifyFlatHeadings(
                tocDivXPath,
                asList(
                        new Heading("Heading one", "#FlatTOC-Headingone"),
                        new Heading("Heading two", "#FlatTOC-Headingtwo")
                )
        );
        assertEquals("{ Heading one } { Heading two }", getElementTextByXPath(tocDivXPath));
    }

    public void testCreateFlatTocWithCommaSeparator()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Flat TOC");
        pageHelper.setContent(
                "{toc:type=flat|separator=comma}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Flat+TOC");

        String tocDivXPath = "//div[@class='wiki-content']/div";

        verifyFlatHeadings(
                tocDivXPath,
                asList(
                        new Heading("Heading one", "#FlatTOC-Headingone"),
                        new Heading("Heading two", "#FlatTOC-Headingtwo")
                )
        );
        assertEquals("Heading one , Heading two", getElementTextByXPath(tocDivXPath));
    }

    public void testCreateFlatTocWithParenthesisSeparator()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Flat TOC");
        pageHelper.setContent(
                "{toc:type=flat|separator=paren}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Flat+TOC");

        String tocDivXPath = "//div[@class='wiki-content']/div";

        verifyFlatHeadings(
                tocDivXPath,
                asList(
                        new Heading("Heading one", "#FlatTOC-Headingone"),
                        new Heading("Heading two", "#FlatTOC-Headingtwo")
                )
        );
        assertEquals("( Heading one ) ( Heading two )", getElementTextByXPath(tocDivXPath));
    }

    public void testCreateFlatTocWithPipeSeparator()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Flat TOC");
        pageHelper.setContent(
                "{toc:type=flat|separator=pipe}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Flat+TOC");

        String tocDivXPath = "//div[@class='wiki-content']/div";

        verifyFlatHeadings(
                tocDivXPath,
                asList(
                        new Heading("Heading one", "#FlatTOC-Headingone"),
                        new Heading("Heading two", "#FlatTOC-Headingtwo")
                )
        );
        assertEquals("Heading one | Heading two", getElementTextByXPath(tocDivXPath));
    }

    public void testCreateFlatTocWithCustomSeparator()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Flat TOC");
        pageHelper.setContent(
                "{toc:type=flat|separator=++}\n" +
                        "h1. Heading one\n" +
                        "h1. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/Flat+TOC");

        String tocDivXPath = "//div[@class='wiki-content']/div";

        verifyFlatHeadings(
                tocDivXPath,
                asList(
                        new Heading("Heading one", "#FlatTOC-Headingone"),
                        new Heading("Heading two", "#FlatTOC-Headingtwo")
                )
        );

        assertTrue(getElementTextByXPath("//div[@class='wiki-content']//div[@class='toc-macro']").contains("++"));
    }

    public void testMinLevelMaxLevel()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Level");
        pageHelper.setContent(
                "{toc:type=list|minLevel=3|maxLevel=3}\n" +
                        "h2. Heading two\n" +
                        "h3. Heading three once\n" +
                        "h3. Heading three again\n" +
                        "h4. Heading four\n" +
                        "h5. Heading five\n"
        );

        assertTrue(pageHelper.create());


        gotoPage("/display/" + testSpaceKey + "/TOC+Level");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading three once", "#TOCLevel-Headingthreeonce"),
                        new Heading("Heading three again", "#TOCLevel-Headingthreeagain")
                )
        );

        assertLinkNotPresentWithText("Heading two");
        assertLinkNotPresentWithText("Heading four");
        assertLinkNotPresentWithText("Heading five");
    }

    public void testLowerCaseMinLevelMaxLevel()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Level");
        pageHelper.setContent(
                "{toc:type=list|minlevel=3|maxlevel=3}\n" +
                        "h2. Heading two\n" +
                        "h3. Heading three once\n" +
                        "h3. Heading three again\n" +
                        "h4. Heading four\n" +
                        "h5. Heading five\n"
        );

        assertTrue(pageHelper.create());


        gotoPage("/display/" + testSpaceKey + "/TOC+Level");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading three once", "#TOCLevel-Headingthreeonce"),
                        new Heading("Heading three again", "#TOCLevel-Headingthreeagain")
                )
        );

        assertLinkNotPresentWithText("Heading two");
        assertLinkNotPresentWithText("Heading four");
        assertLinkNotPresentWithText("Heading five");
    }

    public void testMinLevelMaxLevelWithRange()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Level");
        pageHelper.setContent(
                "{toc:type=list|minLevel=3|maxLevel=5}\n" +
                        "h2. Heading two\n" +
                        "h3. Heading three once\n" +
                        "h3. Heading three again\n" +
                        "h4. Heading four\n" +
                        "h5. Heading five\n"
        );

        assertTrue(pageHelper.create());


        gotoPage("/display/" + testSpaceKey + "/TOC+Level");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading three once", "#TOCLevel-Headingthreeonce"),
                        new Heading("Heading three again", "#TOCLevel-Headingthreeagain",
                                asList(
                                        new Heading("Heading four", "#TOCLevel-Headingfour",
                                                asList(new Heading("Heading five", "#TOCLevel-Headingfive")))
                                )
                        )
                )
        );

        assertLinkNotPresentWithText("Heading two");
    }

    public void testMixCaseMinLevelMaxLevelWithRange()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Level");
        pageHelper.setContent(
                "{toc:type=list|minlevel=3|maxLevel=5}\n" +
                        "h2. Heading two\n" +
                        "h3. Heading three once\n" +
                        "h3. Heading three again\n" +
                        "h4. Heading four\n" +
                        "h5. Heading five\n"
        );

        assertTrue(pageHelper.create());


        gotoPage("/display/" + testSpaceKey + "/TOC+Level");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading three once", "#TOCLevel-Headingthreeonce"),
                        new Heading("Heading three again", "#TOCLevel-Headingthreeagain",
                                asList(
                                        new Heading("Heading four", "#TOCLevel-Headingfour",
                                                asList(new Heading("Heading five", "#TOCLevel-Headingfive")))
                                )
                        )
                )
        );

        assertLinkNotPresentWithText("Heading two");
    }

    public void testMinLevelMaxLevelWithRangeAndSkippedHeadings()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Level");
        pageHelper.setContent(
                "{toc:minLevel=2|maxLevel=3}\n" +
                        "h2. Heading two\n" +
                        "h4. Heading four\n"
        );

        assertTrue(pageHelper.create());


        gotoPage("/display/" + testSpaceKey + "/TOC+Level");


        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading two", "#TOCLevel-Headingtwo")
                )
        );

        assertLinkNotPresentWithText("Heading four");
    }

    /*
     * Anti-test case for <a href="http://developer.atlassian.com/jira/browse/TOC-53">TOC-53</a>
     */
    public void testTocRenderedInExcerptMacro()
    {
        final PageHelper pageHelper = getPageHelper();
        final PageHelper excerptIncludePageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testTocRenderedInExcerptMacro");
        pageHelper.setContent("{excerpt}\n" +
                "Excerpt of child page.\n" +
                "{toc}\n" +
                "{excerpt}\n" +
                "\n" +
                "h1. Heading 1\n" +
                "h2. Heading 1.1\n" +
                "h3. Heading 1.1.1");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        /* Guard condition -- making sure {excerpt} is working "properly" */
        assertTextPresent("Excerpt of child page.");

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading 1", "#testTocRenderedInExcerptMacro-Heading1",
                                asList(
                                        new Heading("Heading 1.1", "#testTocRenderedInExcerptMacro-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", "#testTocRenderedInExcerptMacro-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );

        excerptIncludePageHelper.setSpaceKey(testSpaceKey);
        excerptIncludePageHelper.setTitle("Excerpt Include Page for " + pageHelper.getTitle());
        excerptIncludePageHelper.setContent("{excerpt-include:" + pageHelper.getTitle() + "}");

        assertTrue(excerptIncludePageHelper.create());

        /* Check if excerpt is included properly */
        gotoPage("/pages/viewpage.action?pageId=" + excerptIncludePageHelper.getId());

        assertTextPresent("Excerpt of child page.");

        verifyHeadings(
                "//div[@class='wiki-content']//div[@class='panelContent']/div/ul",
                asList(
                        new Heading("Heading 1", "#ExcerptIncludePagefortestTocRenderedInExcerptMacro-Heading1",
                                asList(
                                        new Heading("Heading 1.1", "#ExcerptIncludePagefortestTocRenderedInExcerptMacro-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", "#ExcerptIncludePagefortestTocRenderedInExcerptMacro-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );
    }

    public void testTocRenderedWithAbsoluteURlsInExcerptMacro()
    {
        final PageHelper pageHelper = getPageHelper();
        final PageHelper excerptIncludePageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testTocRenderedInExcerptMacro");
        pageHelper.setContent("{excerpt}\n" +
                "Excerpt of child page.\n" +
                "{toc:absoluteUrl=true}\n" +
                "{excerpt}\n" +
                "\n" +
                "h1. Heading 1\n" +
                "h2. Heading 1.1\n" +
                "h3. Heading 1.1.1");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        /* Guard condition -- making sure {excerpt} is working "properly" */
        assertTextPresent("Excerpt of child page.");

        String absoluteUrl = getAbsoluteUrl(pageHelper);

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading 1", absoluteUrl + "#testTocRenderedInExcerptMacro-Heading1",
                                asList(
                                        new Heading("Heading 1.1", absoluteUrl + "#testTocRenderedInExcerptMacro-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", absoluteUrl + "#testTocRenderedInExcerptMacro-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );

        excerptIncludePageHelper.setSpaceKey(testSpaceKey);
        excerptIncludePageHelper.setTitle("Excerpt Include Page for " + pageHelper.getTitle());
        excerptIncludePageHelper.setContent("{excerpt-include:" + pageHelper.getTitle() + "}");

        assertTrue(excerptIncludePageHelper.create());

        /* Check if excerpt is included properly */
        gotoPage("/pages/viewpage.action?pageId=" + excerptIncludePageHelper.getId());

        assertTextPresent("Excerpt of child page.");

        verifyHeadings(
                "//div[@class='wiki-content']//div[@class='panelContent']/div/ul",
                asList(
                        new Heading("Heading 1", absoluteUrl + "#testTocRenderedInExcerptMacro-Heading1",
                                asList(
                                        new Heading("Heading 1.1", absoluteUrl + "#testTocRenderedInExcerptMacro-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", absoluteUrl + "#testTocRenderedInExcerptMacro-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );
    }

    private String getAbsoluteUrl(PageHelper pageHelper)
    {
        return tester.getTestContext().getBaseUrl() + "display/" + pageHelper.getSpaceKey() + "/" + pageHelper.getTitle();
    }

    public void testTocLinksInIncludeMacro()
    {
        final PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Page");
        pageHelper.setContent(
                "{toc}\n" +
                        "h1. Heading 1\n" +
                        "h2. Heading 1.1\n" +
                        "h3. Heading 1.1.1");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading 1", "#TOCPage-Heading1",
                                asList(
                                        new Heading("Heading 1.1", "#TOCPage-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", "#TOCPage-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );

        final PageHelper includePageHelper = getPageHelper();
        includePageHelper.setSpaceKey(testSpaceKey);
        includePageHelper.setTitle("Include Page for " + pageHelper.getTitle());
        includePageHelper.setContent("{include:" + pageHelper.getTitle() + "}");

        assertTrue(includePageHelper.create());

        /* Check if page is included properly */
        gotoPage("/pages/viewpage.action?pageId=" + includePageHelper.getId());

        // Links should have containing page's anchor
        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading 1", "#IncludePageforTOCPage-Heading1",
                                asList(
                                        new Heading("Heading 1.1", "#IncludePageforTOCPage-Heading1.1",
                                                asList(
                                                        new Heading("Heading 1.1.1", "#IncludePageforTOCPage-Heading1.1.1")
                                                )
                                        )
                                )
                        )
                )
        );
    }

    public void testDuplicateTocLinksInIncludeMacro()
    {
        final PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC Page");
        pageHelper.setContent(
                "{toc}\n" +
                        "h1. Heading 1\n" +
                        "h1. Heading 1");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading 1", "#TOCPage-Heading1"),
                        new Heading("Heading 1", "#TOCPage-Heading1.1")
                )
        );

        final PageHelper includePageHelper = getPageHelper();
        includePageHelper.setSpaceKey(testSpaceKey);
        includePageHelper.setTitle("Include Page for " + pageHelper.getTitle());
        includePageHelper.setContent("{include:" + pageHelper.getTitle() + "}");

        assertTrue(includePageHelper.create());

        /* Check if page is included properly */
        gotoPage("/pages/viewpage.action?pageId=" + includePageHelper.getId());

        // Links should have containing page's anchor
        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("Heading 1", "#IncludePageforTOCPage-Heading1"),
                        new Heading("Heading 1", "#IncludePageforTOCPage-Heading1.1")
                )
        );
    }

    private boolean isHtmlMacroPluginEnabled() throws SAXException
    {
        try
        {
            gotoPageWithEscalatedPrivileges("/admin/viewplugins.action");

            clickLinkWithText("Confluence HTML Macros");

            return !getDialog().hasElementByXPath("//a[text()='Enable plugin']");
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    private void enableHtmlMacroPlugin() throws SAXException
    {
        try
        {
            gotoPageWithEscalatedPrivileges("/admin/console.action");
            clickLinkWithText("Plugins");
            clickLinkWithText("Confluence HTML Macros");
            clickLinkWithText("Enable plugin");
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    private void enableHtmlModule() throws SAXException
    {
        try
        {
            gotoPageWithEscalatedPrivileges("/admin/viewplugins.action");
            clickLinkWithText("Confluence HTML Macros");


            String atlassianToken = getElementAttributeByXPath("//meta[@id='atlassian-token']", "content");
            gotoPage("/admin/enableplugin.action?atl_token=" + atlassianToken + "&moduleKey=confluence.macros.html%3Ahtml");
            gotoPage("/admin/enableplugin.action?atl_token=" + atlassianToken + "&moduleKey=confluence.macros.html%3Ahtml-xhtml");
        }
        finally
        {
            dropEscalatedPrivileges();
        }
    }

    public void testTocIsGeneratedWhenHeadingDoesNotContainAnchor() throws SAXException
    {
        final PageHelper pageHelper = getPageHelper();

        if (!isHtmlMacroPluginEnabled())
        {
            enableHtmlMacroPlugin();
            enableHtmlModule();
        }
        else
        {
            enableHtmlModule();
        }

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testTocIsGeneratedWhenHeadingDoesNotContainAnchor");
        pageHelper.setContent("{toc:outline=true}\n" +
                "\n" +
                "h1.My Tasks\n" +
                "\n" +
                "{html}\n" +
                "<h2>\n" +
                "\n" +
                "\n" +
                "Task To Do\n" +
                "\n" +
                "\n" +
                "</h2>\n" +
                "{html}\n" +
                "\n" +
                "{html}\n" +
                "<h2>\n" +
                "\n" +
                "\n" +
                "Task Done\n" +
                "\n" +
                "\n" +
                "</h2>\n" +
                "{html}\n" +
                "\n" +
                "h1.{color:blue}Normal Confluence Heading{color}");

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("1", "My Tasks", "#testTocIsGeneratedWhenHeadingDoesNotContainAnchor-MyTasks",
                                asList(
                                        new Heading("1.1", "Task To Do", (String) null),
                                        new Heading("1.2", "Task Done", (String) null)
                                )
                        ),
                        new Heading("2", "Normal Confluence Heading", "#testTocIsGeneratedWhenHeadingDoesNotContainAnchor-NormalConfluenceHeading")
                )
        );
    }

    /*
     * TOC-71
     */
    public void testTocGenerationWithAccentedCharacters()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testTocGenerationWithAccentedCharacters");
        pageHelper.setContent("{toc}\n" +
                "\n" +
                "h1. \u00c4\u00e4kk\u00f6nen"); /* See TOC-71 */

        assertTrue(pageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
       
        verifyHeadings(
                "//div[@class='wiki-content']/div/ul",
                asList(
                        new Heading("\u00c4\u00e4kk\u00f6nen", "#testTocGenerationWithAccentedCharacters-\u00c4\u00e4kk\u00f6nen")
                )
        );
    }

    /* https://studio.plugins.atlassian.com/browse/TOC-113 */
    public void testStyleAttributeEscaped()
    {
        PageHelper pageHelper = getPageHelper();
        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testStyleAttributeEscaped");
        pageHelper.setContent("{toc:style=margin-left: 0px;\\}</style><script>alert(document.cookie)</script>:xx}\n" +
                "h2. One\n" +
                "{loremipsum}\n" +
                "h2. Two\n" +
                "{loremipsum}");

        assertTrue(pageHelper.create());
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertElementNotPresentByXPath("//div[@class='wiki-content']//script");
    }
    
    /*
     * https://studio.plugins.atlassian.com/browse/TOC-137
     */
    public void testTocDoesNotMakeTasklistBlank()
    {
    	final PageHelper pageHelper = getPageHelper();
    	
    	pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("testTocDoesNotMakeTasklistBlank");
        pageHelper.setContent("{toc}\n" +
        		"{tasklist}\n" + 
        		"||Completed||Priority||Locked||CreatedDate||CompletedDate||Assignee||Name||\n" + 
        		"|F|M|F|1332829662215|          |admin|task 1|\n" + 
        		"{tasklist}");
        
        assertTrue(pageHelper.create());
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());
        // we need to refresh the page to make the tasks appear in tasklist macro
        gotoPage("/pages/viewpage.action?pageId=" + pageHelper.getId());

        assertEquals(
                "task 1",
                getElementTextByXPath("//div[@class=' none-complete  task-list']/ol[@class='tasklist-container']/li//p[@class='rendered taskname']")
        );
        
    }

    /*
     * https://ecosysem.atlassian.net/browse/TOC-143
     */
    public void testTocRenderedWithHeaderAnchorAndWeblink()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);

        pageHelper.setTitle("Test rendered TOC with anchor and weblink");
        pageHelper.setContent(
                "{toc}\n" +
                        "h2. {anchor:heading2}heading 2 title\n" +
                        "[Link to heading 2|#heading2]"

        );

        assertTrue(pageHelper.create());

        gotoPage(this.getConfluenceWebTester().getBaseUrl() + "/pages/viewpage.action?pageId=" + pageHelper.getId());

        // make sure <span class="confluence-anchor-link"> exists because we need to extract the header name based on it
        // in net.customware.confluence.plugin.toc.OutlineRendered#extractHeadingName
        assertElementPresentByXPath("//h2[@id='TestrenderedTOCwithanchorandweblink-heading2heading2title']/span[@class='confluence-anchor-link']");
        assertEquals("heading 2 title", getElementTextByXPath("//h2[@id='TestrenderedTOCwithanchorandweblink-heading2heading2title']"));

        // make sure toc uses the correct header name by not including the anchor link class
        assertElementPresentByXPath("//ul[@class='toc-indentation']/li/a[@href='#TestrenderedTOCwithanchorandweblink-heading2heading2title']");
        assertEquals("heading 2 title",getElementTextByXPath("//ul[@class='toc-indentation']/li/a[text()]"));
    }

    /*
     * https://ecosysem.atlassian.net/browse/TOC-167
     */
    public void testCssClassNameDoesNotCauseXss()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with CSS Class");
        pageHelper.setContent(
                "{toc:class=' style='top: 0; background-color: #f00' onmouseover='alert(1)}\n" +
                        "h1. Heading one\n" +
                        "h2. Heading two\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+CSS+Class");

        assertElementNotPresentByXPath("//div[@onmouseover='alert(1)']");

    }

    public void verifyHeadings(String tocListXPath, List<Heading> headings)
    {
        tocTester.verifyHeadings(tocListXPath, headings);
    }

    public void verifyFlatHeadings(String tocListXPath, List<Heading> headings)
    {
        tocTester.verifyFlatHeadings(tocListXPath, headings);
    }
}
