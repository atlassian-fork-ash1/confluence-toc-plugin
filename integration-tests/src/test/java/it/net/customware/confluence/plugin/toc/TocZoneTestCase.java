package it.net.customware.confluence.plugin.toc;

import com.atlassian.confluence.plugin.functest.AbstractConfluencePluginWebTestCase;
import com.atlassian.confluence.plugin.functest.helper.PageHelper;

import java.util.Arrays;
import java.util.List;

public class TocZoneTestCase extends AbstractConfluencePluginWebTestCase
{
    private TocTester tocTester;
    private String testSpaceKey;

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();
        tocTester = new TocTester(tester);
        testSpaceKey = tocTester.createTestSpace();
    }

    public void testCreateTocWithNonSequentialHeadersStartingFromH2()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with non sequential headings starting from h2");
        pageHelper.setContent(
                "h2. Heading A\n" +
                        "{toc-zone:class=toc-zone}\n" +
                        "h4. Heading B\n" +
                        "h4. Heading C\n" +
                        "h6. Heading D\n" +
                        "{toc-zone}\n" +
                        "h2. Heading E\n" +
                        "h4. Heading F\n"
        );

        assertTrue(pageHelper.create());
    
        gotoPage("/display/" + testSpaceKey + "/TOC+with+non+sequential+headings+starting+from+h2");

        // At the top
        verifyFlatHeadings(
                "//div[@class='wiki-content']//div[contains(@class,'toc-zone')][1]",
                Arrays.asList(
                        new Heading("Heading B", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingB"),
                        new Heading("Heading C", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingC"),
                        new Heading("Heading D", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingD")
                )
        );
        assertElementPresentByXPath("//div[contains(@class,'toc-zone')]/following-sibling::h4");

        // At the bottom
        verifyFlatHeadings(
                "//div[@class='wiki-content']//div[contains(@class,'toc-zone')][2]",
                Arrays.asList(
                        new Heading("Heading B", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingB"),
                        new Heading("Heading C", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingC"),
                        new Heading("Heading D", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingD")
                )
        );
    }

    public void testCreateTocAtTopLocation()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with non sequential headings starting from h2");
        pageHelper.setContent(
                "h2. Heading A\n" +
                        "{toc-zone:class=toc-zone|location=top}\n" +
                        "h4. Heading B\n" +
                        "h4. Heading C\n" +
                        "h6. Heading D\n" +
                        "{toc-zone}\n" +
                        "h2. Heading E\n" +
                        "h4. Heading F\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+non+sequential+headings+starting+from+h2");


        // At the top
        verifyFlatHeadings(
                "//div[contains(@class,'toc-zone')]",
                Arrays.asList(
                        new Heading("Heading B", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingB"),
                        new Heading("Heading C", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingC"),
                        new Heading("Heading D", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingD")
                )
        );
        // Verify that a toc is not generated at the bottom
        assertElementNotPresentByXPath("//div[@class='wiki-content']/h4/following-sibling::div[@class='toc-zone']");
    }

    public void testCreateTocAtBottomLocation()
    {
        final PageHelper pageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("TOC with non sequential headings starting from h2");
        pageHelper.setContent(
                "h2. Heading A\n" +
                        "{toc-zone:class=toc-zone|location=bottom}\n" +
                        "h4. Heading B\n" +
                        "h4. Heading C\n" +
                        "h6. Heading D\n" +
                        "{toc-zone}\n" +
                        "h2. Heading E\n" +
                        "h4. Heading F\n"
        );

        assertTrue(pageHelper.create());

        gotoPage("/display/" + testSpaceKey + "/TOC+with+non+sequential+headings+starting+from+h2");


        assertElementNotPresentByXPath("//div[contains(@class,'toc-zone')]/following-sibling::h6");

        // At the bottom
        verifyFlatHeadings(
                "//div[@class='wiki-content']//h4/following-sibling::div[contains(@class,'toc-zone')]",
                Arrays.asList(
                        new Heading("Heading B", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingB"),
                        new Heading("Heading C", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingC"),
                        new Heading("Heading D", "#TOCwithnonsequentialheadingsstartingfromh2-HeadingD")
                )
        );
    }

    public void testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed()
    {
        final PageHelper pageHelper = getPageHelper();
        final PageHelper tocZonePageHelper = getPageHelper();

        pageHelper.setSpaceKey(testSpaceKey);
        pageHelper.setTitle("Target page");
        pageHelper.setContent("Target page content");

        assertTrue(pageHelper.create());

        tocZonePageHelper.setSpaceKey(testSpaceKey);
        tocZonePageHelper.setTitle("testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed");
        tocZonePageHelper.setContent("{toc-zone:class=toc-zone}\n" +
                "\n" +
                "h1.Heading 1\n" +
                "h2.Heading 2\n" +
                "h3.Heading 3\n" +
                "h4.[Target page|Target page]\n" +
                "\n" +
                "{toc-zone}");

        assertTrue(tocZonePageHelper.create());

        gotoPage("/pages/viewpage.action?pageId=" + tocZonePageHelper.getId());

        verifyFlatHeadings(
                "//div[@class='toc-macro toc-zone']",
                Arrays.asList(
                        new Heading("Heading 1", "#testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed-Heading1"),
                        new Heading("Heading 2", "#testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed-Heading2"),
                        new Heading("Heading 3", "#testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed-Heading3"),
                        new Heading("Target page", "#testIfWikiLinksInTocZoneBodyIsRefactoredIfTargetPageIsRenamed-Targetpage")
                )
        );

        // Rename page
        clickLink("editPageLink");
        setWorkingForm("editpageform");
        setTextField("title", "Test page renamed");

        clickButton("rte-button-publish");

        gotoPage("/pages/viewpage.action?pageId=" + tocZonePageHelper.getId());

        verifyFlatHeadings(
                "//div[@class='toc-macro toc-zone']",
                Arrays.asList(
                        new Heading("Heading 1", "#Testpagerenamed-Heading1"),
                        new Heading("Heading 2", "#Testpagerenamed-Heading2"),
                        new Heading("Heading 3", "#Testpagerenamed-Heading3"),
                        new Heading("Target page", "#Testpagerenamed-Targetpage")
                )
        );
    }

    private void verifyFlatHeadings(String tocListXPath, List<Heading> headings)
    {
        tocTester.verifyFlatHeadings(tocListXPath, headings);
    }
}
